# COVID-19 modelling school closure in Switzerland

This repository contains scripts used in the analysis presented in the manuscript _Endemic-epidemic modelling for school
closure to prevent spread of COVID-19 in Switzerland_

## Comparison with earlier analysis for Zurich

### Inputs that deviate from Zurich analysis:
* Different data file
* Different population
  * demo_pjan data set for population by age group in the entire country
  * demo_r_pjangrp3 data set for population by canton (used as population weights for calculation of "holiday score")
* Public holidays and school holidays (calculated a notional "holiday score" as holidays differ between and sometimes even within cantons)
  * Info on how exactly this is done can be found on page 11 of the registered report under [this link](https://gitlab.switch.ch/suspend/suspend/-/blob/master/school-closure/resubmission-swiss-analysis/protocol2.pdf)
* Temperature is based on the same data set. However, we do not only use the data for the canton of Zurich but rather average all cantons.
* Testing rate also differs. The file we used for the Zurich analysis only provides data until 4th of October. Therefore we also use the data provided by the BAG API. This data provides data on tests for all days since 23rd of May. Therefore, from 24th of January (start of the csv file from the Zurich analysis) until the 22nd of May we use the excel list. For later days we use the data aquired via the API.

### Inputs that do not deviate:

* Indicators
  * We decided at some point that we would only consider national measures (otherwise too sensitive to measures implemented in some cantons)
  * As the canton Zurich did not impose stricter measures than the federal government during our study period we have the same indicators
* Testing rate: This is at national level anyway.

