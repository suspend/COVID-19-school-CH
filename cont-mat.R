# Run setup
source("setup.R")
source("aux-func.R")

# should plots be created
do_plots <- FALSE

# Load contact data ------------------------------------------------------------
load(file = "data/contact-matrices.Rda")
dm <- unique(c(sapply(list(m_household, m_school, m_work, m_other), dim))) # used in test of the grouping variable
if(length(dm) != 1L) stop("Error in dimension of contact matrices")

# Load Mistry et al. weights ---------------------------------------------------
load("data/cont-weights.Rda")

# Load population data ---------------------------------------------------------
load("data/population.Rda")

# Load policy data -------------------------------------------------------------
load("data/policy.Rda")

# Truncate all of them to our study period
policy <- policy[policy$date %in% case_dates, ]

# Grouping variable for aggregation --------------------------------------------

# Grouping required for aggregating matrices
# Determines the size of the age groups considered
# This is needed for the preprocessing of the school matrix
# (incorporating school holidays)
# as well as the aggregation of contact matrices to age groups
grouping <- vapply(labls, function(x){
  # get start and end age of the group based on the location of the dash
  # in the label (which is defined based on the object brk from setup.R)
  start_end <- unlist(strsplit(x, "-"))
  if(length(start_end) == 1){
    start_end <- c(start_end, dm - 1)
  }
  # convert to numeric
  start_end <- as.numeric(gsub("\\D", "", start_end))
  # return length of the sequence from start to end
  return(length(seq(start_end[1], start_end[2], by = 1)))
}, numeric(1L), USE.NAMES = FALSE)
# Run check
source("tests/test-grouping.R")

# Aggregate matrices -----------------------------------------------------------

# This is actually possible and yields the same results but runs much faster
# See commented out test below for a demo
m_school <- aggregate_matrix(m_school)
m_other <- aggregate_matrix(m_other)
m_household <- aggregate_matrix(m_household)
m_work <- aggregate_matrix(m_work)

# Load school holidays ---------------------------------------------------------
load("data/school-hols.Rda")
# Contact levels are 0 if it is a school holiday
scl <- sch$weight

# Construct weight arrays ------------------------------------------------------

# create dimension names for the final arrays
dim_names <- append(dimnames(m_household), list(day = as.character(case_dates)))

# dimension of the final matrix for each day
dm_final <- length(grouping)

# make arrays: 
ideas <- c("original", "adj_indicator", "adj_weights")
policy_inds <- rep("policy", 3)

# loop over sensitivity methods
weights <- lapply(seq_along(ideas), function(x){
  
  # choose correct set of indicators
  indicators <- eval(as.name(policy_inds[x]))
  factor <- if(ideas[x] == "adj_weights") 0.5 else NULL
  ### Scenario A: ###
  
  # Apply contact restrictions (school indicator) to all age groups in the "as is" scenario (A)
  
  # total = school-weight * school-indicator * school-matrix * school-holiday_score + 
  #         household-weight * household-indicator * household-matrix +
  #         work-weight * work-indicator * work-matrix +
  #         other-weight * other-indicator * other-matrix
  
  arrsA <- vapply(seq_along(case_dates), function(t){
    # Calculate setting specific matrices for day t
    day_mat <- lapply(list("school", "work", "household", "other"), function(setting){
      # get indicator, weight, matrix
      vals <- get_values(method = ideas[x], setting = setting, t = t, 
                         indicators = indicators, mistry = mistry, scl = scl,
                         factor = factor)
      # combine everything
      calculate_a(setting = setting, weight = vals$weight, matrix = vals$matrix,
                  school_holiday_score = vals$school_holiday_score, 
                  indicator = vals$indicator)
    })
    # Sum up matrices for day t # and aggregate to our age groups
    Reduce(`+`, day_mat)
    #aggregate_matrix(Reduce(`+`, day_mat))
  }, matrix(NA_real_, dm_final, dm_final))
  
  ### Scenario B: ###
  
  # Apply contact restrictions (school indicator) only to older age groups in scenario B (schools always open)
  # this is equivalent to school_indicator = 1 on all days for youngest age groups
  
  # total = school-weight * school-indicator(modified) * school-matrix * school-holiday_score + 
  #         household-weight * household-indicator * household-matrix +
  #         work-weight * work-indicator * work-matrix +
  #         other-weight * other-indicator * other-matrix
  
  not_youngest_idx <- 2:dm_final
  #not_youngest_idx <- (grouping[1] + 1):dm # indices of age groups that do not belong to the youngest age groups
  
  arrsB <- vapply(seq_along(case_dates), function(t){
    # Calculate setting specific matrices for day t
    day_mat <- lapply(list("school", "work", "household", "other"), function(setting){
      # get indicator, weight, matrix
      vals <- get_values(method = ideas[x], setting = setting, t = t, 
                         indicators = indicators, mistry = mistry, scl = scl,
                         factor = factor)
      # combine everything
      calculate_b(setting = setting, weight = vals$weight, matrix = vals$matrix,
                  school_holiday_score = vals$school_holiday_score, 
                  indicator = vals$indicator, not_youngest_idx = not_youngest_idx)
    })
    # Sum up matrices for day t and aggregate to our age groups
    Reduce(`+`, day_mat)
    #aggregate_matrix(Reduce(`+`, day_mat))
  }, matrix(NA_real_, dm_final, dm_final))
  
  ### Scenario C: ###
  
  # Apply contact restrictions (school indicator) only to the older age groups in scenario C (schools always closed)
  # Also apply indicator to younger age group until the first time schools are closed
  # After that, never reopen schools (set school contacts for youngest age to 0)
  # while keep using indicator for older age groups
  
  # total = school-weight * school-indicator(modified) * school-matrix * school-holiday_score + 
  #         household-weight * household-indicator * household-matrix +
  #         work-weight * work-indicator * work-matrix +
  #         other-weight * other-indicator * other-matrix
  
  school_closing_date <- which(policy$C1_school == 0)[1] # Date of school closure
  
  arrsC <- vapply(seq_along(case_dates), function(t){
    # Calculate setting specific matrices for day t
    day_mat <- lapply(list("school", "work", "household", "other"), function(setting){
      # get indicator, weight, matrix, school_holiday_score
      vals <- get_values(method = ideas[x], setting = setting, t = t, 
                         indicators = indicators, mistry = mistry, scl = scl,
                         factor = factor)
      # combine everything
      calculate_c(setting = setting, weight = vals$weight, matrix = vals$matrix,
                  school_holiday_score = vals$school_holiday_score, 
                  indicator = vals$indicator, t = t, 
                  school_closing_date = school_closing_date,
                  not_youngest_idx = not_youngest_idx)
    })
    # Sum up matrices for day t and aggregate to our age groups
    Reduce(`+`, day_mat)
    #aggregate_matrix(Reduce(`+`, day_mat))
  }, matrix(NA_real_, dm_final, dm_final))
  
  # set dimension names for the three arrays
  dimnames(arrsA) <- dimnames(arrsB) <- dimnames(arrsC) <- dim_names
  
  # Return list of all three arrays
  list("scenario_a" = arrsA, "scenario_b" = arrsB, "scenario_c" = arrsC)
})
names(weights) <- ideas

# Plots ---------------------------------------------------------------------------------------

if(do_plots){
  # Sequence of contact matrices to be plotted to show changes over time
  # Starting with first day which is not a school holiday as else change
  # might not be as apparant to reader
  inx <- floor(seq(which(scl == 1)[1], length(case_dates), length.out = 10))
  # This is used as an example to illustrate
  scl[inx[5]] # 1 so not a school holiday -- contact levels remain as they were
  # Show specific date for illustration purposes
  # pdf(paste0("figures/", suffix, "construction-illustration1.pdf"),
  #     width = 6, height = 4)
  ggplot(data = policy) +
    geom_line(aes(x = date, y = C1_school + 0.01,
                  colour = "School closure status")) +
    geom_line(aes(x = date, y = C2_work + 0.005,
                  colour = "Remote work status")) +
    geom_line(aes(x = date, y = C4_other - 0.005,
                  colour = "Restrictions on gatherings (other)")) +
    geom_line(aes(x = date, y = C6_household - 0.01,
                  colour = "Restrictions on household contacts")) +
    geom_point(data = policy[policy$date == case_dates[inx[5]], ],
               aes(x = date, y = C1_school + 0.01), size = 1) +
    geom_point(data = policy[policy$date == case_dates[inx[5]], ],
               aes(x = date, y = C2_work + 0.005), size = 1) +
    geom_point(data = policy[policy$date == case_dates[inx[5]], ],
               aes(x = date, y = C4_other - 0.005), size = 1) +
    geom_point(data = policy[policy$date == case_dates[inx[5]], ],
               aes(x = date, y = C6_household - 0.01), size = 1) +
    annotate("text", x = policy[policy$date == case_dates[inx[5]], ]$date,
             y = policy[policy$date == case_dates[inx[5]], ]$C2_work + 0.04,
             label = case_dates[inx[5]]) +
    labs(y = "Policy indicator",
         x = "", colour = "Measure") +
    theme(legend.position = "bottom")
  ggsave(paste0("figures/", suffix, "construction-illustration1.pdf"),
         width = 6, height = 4)
}

# Use simulated weights -----------------------------------------------------------------------

# loop over policy indicators
sim_weights <- lapply(seq_along(ideas), function(x){
  
  # choose correct set of indicators
  indicators <- eval(as.name(policy_inds[x]))
  factor <- if(ideas[x] == "adj_weights") 0.5 else NULL
  ### Scenario A: ###
  
  # Apply contact restrictions (school indicator) to all age groups in the "as is" scenario (A)
  
  # total = school-weight * school-indicator * school-matrix * school-holiday_score + 
  #         household-weight * household-indicator * household-matrix +
  #         work-weight * work-indicator * work-matrix +
  #         other-weight * other-indicator * other-matrix
  
  arrsA <- lapply(seq_len(nrow(sim_mistry)), function(y){
    simulated_weights <- unlist(sim_mistry[y, ], use.names = TRUE)
    out <- vapply(seq_along(case_dates), function(t){
      # Calculate setting specific matrices for day t
      day_mat <- lapply(list("school", "work", "household", "other"), function(setting){
        # get indicator, weight, matrix
        vals <- get_values(method = ideas[x], setting = setting, t = t, 
                           indicators = indicators, mistry = simulated_weights, scl = scl, 
                           factor = factor)
        # combine everything
        calculate_a(setting = setting, weight = vals$weight, matrix = vals$matrix,
                    school_holiday_score = vals$school_holiday_score, 
                    indicator = vals$indicator)
      })
      # Sum up matrices for day t # and aggregate to our age groups
      Reduce(`+`, day_mat)
      #aggregate_matrix(Reduce(`+`, day_mat))
    }, matrix(NA_real_, dm_final, dm_final))
    dimnames(out) <- dim_names
    out
  })
  
  ### Scenario B: ###
  
  # Apply contact restrictions (school indicator) only to older age groups in scenario B (schools always open)
  # this is equivalent to school_indicator = 1 on all days for youngest age groups
  
  # total = school-weight * school-indicator(modified) * school-matrix * school-holiday_score + 
  #         household-weight * household-indicator * household-matrix +
  #         work-weight * work-indicator * work-matrix +
  #         other-weight * other-indicator * other-matrix
  
  not_youngest_idx <- 2:dm_final
  #not_youngest_idx <- (grouping[1] + 1):dm # indices of age groups that do not belong to the youngest age groups
  
  arrsB <- lapply(seq_len(nrow(sim_mistry)), function(y){
    simulated_weights <- unlist(sim_mistry[y, ], use.names = TRUE)
    out <- vapply(seq_along(case_dates), function(t){
      # Calculate setting specific matrices for day t
      day_mat <- lapply(list("school", "work", "household", "other"), function(setting){
        # get indicator, weight, matrix
        vals <- get_values(method = ideas[x], setting = setting, t = t, 
                           indicators = indicators, mistry = simulated_weights, scl = scl, 
                           factor = factor)
        # combine everything
        calculate_b(setting = setting, weight = vals$weight, matrix = vals$matrix,
                   school_holiday_score = vals$school_holiday_score, 
                   indicator = vals$indicator, not_youngest_idx = not_youngest_idx)
      })
      # Sum up matrices for day t # and aggregate to our age groups
      Reduce(`+`, day_mat)
      #aggregate_matrix(Reduce(`+`, day_mat))
    }, matrix(NA_real_, dm_final, dm_final))
    dimnames(out) <- dim_names
    out
  })
  
  ### Scenario C: ###
  
  # Apply contact restrictions (school indicator) only to the older age groups in scenario C (schools always closed)
  # Also apply indicator to younger age group until the first time schools are closed
  # After that, never reopen schools (set school contacts for youngest age to 0)
  # while keep using indicator for older age groups
  
  # total = school-weight * school-indicator(modified) * school-matrix * school-holiday_score + 
  #         household-weight * household-indicator * household-matrix +
  #         work-weight * work-indicator * work-matrix +
  #         other-weight * other-indicator * other-matrix
  
  school_closing_date <- which(policy$C1_school == 0)[1] # Date of school closure
  
  arrsC <- lapply(seq_len(nrow(sim_mistry)), function(y){
    simulated_weights <- unlist(sim_mistry[y, ], use.names = TRUE)
    out <- vapply(seq_along(case_dates), function(t){
      # Calculate setting specific matrices for day t
      day_mat <- lapply(list("school", "work", "household", "other"), function(setting){
        # get indicator, weight, matrix
        vals <- get_values(method = ideas[x], setting = setting, t = t, 
                           indicators = indicators, mistry = simulated_weights, scl = scl, 
                           factor = factor)
        # combine everything
        calculate_c(setting = setting, weight = vals$weight, matrix = vals$matrix,
                    school_holiday_score = vals$school_holiday_score, 
                    indicator = vals$indicator, t = t, 
                    school_closing_date = school_closing_date,
                    not_youngest_idx = not_youngest_idx)
      })
      # Sum up matrices for day t # and aggregate to our age groups
      Reduce(`+`, day_mat)
      #aggregate_matrix(Reduce(`+`, day_mat))
    }, matrix(NA_real_, dm_final, dm_final))
    dimnames(out) <- dim_names
    out
  })
  
  # Return list of all three arrays
  list("scenario_a" = arrsA, "scenario_b" = arrsB, "scenario_c" = arrsC)
})
names(sim_weights) <- ideas

# Save to file -------------------------------------------

save(weights, file = "data/neweights.Rda")
save(sim_weights, file = "data/sim_neweights.Rda")


# Test whether aggregating first gives same results -------------------------------------------

# rm(list = ls())
# source("setup.R")
# source("aux-func.R")
# load("data/contact-matrices.Rda")
# load("data/cont-weights.Rda")
# load("data/policy.Rda")
# load("data/population.Rda")
# load("data/school-hols.Rda")
# scl <- sch$weight
# dm <- ncol(m_school)
# dm_final <- length(labls)
# grouping <- vapply(labls, function(x){
#   # get start and end age of the group based on the location of the dash
#   # in the label (which is defined based on the object brk from setup.R)
#   start_end <- unlist(strsplit(x, "-"))
#   if(length(start_end) == 1){
#     start_end <- c(start_end, dm - 1)
#   }
#   # convert to numeric
#   start_end <- as.numeric(gsub("\\D", "", start_end))
#   # return length of the sequence from start to end
#   return(length(seq(start_end[1], start_end[2], by = 1)))
# }, numeric(1L), USE.NAMES = FALSE)
# 
# # aggregate in the end
# arrsA <- vapply(seq_along(case_dates), function(t){
#   # Calculate setting specific matrices for day t
#   day_mat <- lapply(list("school", "work", "household", "other"), function(setting){
#     # get indicator, weight, matrix
#     indicator <- policy[[grep(setting, names(policy))]][t] # get indicator value at day t
#     weight <- mistry[setting] # get mistry weight
#     matrix <- eval(as.name(paste0("m_", setting)), envir = .GlobalEnv) # get correct matrix (indep of time)
#     school_holiday_score <- scl[t] # get school holiday score at day t
#     # combine everything
#     calculate_a(setting = setting, weight = weight, matrix = matrix,
#                 school_holiday_score = school_holiday_score, 
#                 indicator = indicator)
#   })
#   # Sum up matrices for day t and aggregate to our age groups
#   aggregate_matrix(Reduce(`+`, day_mat))
# }, matrix(NA_real_, dm_final, dm_final))
# 
# # aggregate in the beginning
# arrsA1 <- vapply(seq_along(case_dates), function(t){
#   # Calculate setting specific matrices for day t
#   day_mat <- lapply(list("school", "work", "household", "other"), function(setting){
#     # get indicator, weight, matrix
#     indicator <- policy[[grep(setting, names(policy))]][t] # get indicator value at day t
#     weight <- mistry[setting] # get mistry weight
#     matrix <- aggregate_matrix(eval(as.name(paste0("m_", setting)), envir = .GlobalEnv)) # get correct matrix (indep of time)
#     school_holiday_score <- scl[t] # get school holiday score at day t
#     # combine everything
#     calculate_a(setting = setting, weight = weight, matrix = matrix,
#                 school_holiday_score = school_holiday_score, 
#                 indicator = indicator)
#   })
#   # Sum up matrices for day t and aggregate to our age groups
#   Reduce(`+`, day_mat)
# }, matrix(NA_real_, dm_final, dm_final))
# 
# all.equal(arrsA, arrsA1) # actually the same
