default: all

# define variables (stuff that is produced by the same script)
holidays = data/school-hols.Rda data/hols.Rda # check
temperature = data/temperature.Rda #check
raw-contact-mats = data/contact-matrices.Rda #check
population = data/population.Rda # check
mistry-weights = data/cont-weights.Rda # check
policy-indicators = data/policy.Rda #check
ne-weights = data/neweights.Rda data/sim_neweights.Rda #check
covariates = covariate_options.Rda
models = models.Rda model_overview.Rda predictions.Rda
simulation-scenarios = model_simulations.Rda
report = analysis.pdf

# define rules for preprocessing
$(raw-contact-mats): setup.R aux-func.R
	Rscript import_contact_mats.R
	
$(holidays): setup.R aux-func.R
	Rscript publ-hol.R
	
$(temperature): setup.R
	Rscript temperature.R
	
$(mistry-weights): setup.R aux-func.R $(raw-contact-mats)
	Rscript mistry_weights.R
	
$(population): setup.R aux-func.R $(raw-contact-mats)
	Rscript make_population.R

$(policy-indicators): setup.R aux-func.R
	Rscript ch_indicators.R

$(ne-weights): setup.R aux-func.R $(raw-contact-mats) $(holidays) $(mistry-weights) $(population) $(policy-indicators)
	Rscript cont-mat.R

# define rules for analysis
$(covariates): setup.R $(temperature) $(holidays) $(population)
	Rscript data-switzerland.R

$(models): setup.R $(covariates)
	Rscript models.R
	
$(simulation-scenarios): setup.R aux-func.R $(models)
	Rscript simulation_scenarios.R


