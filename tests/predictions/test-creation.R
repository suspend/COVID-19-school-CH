# Tests that check that the endemic-epidemic models are in the format required
# for us to calculate the path forecasts

print(expect_true(isTRUE(all.equal(ch_modA, ch_mod)),
                  info = "Best fitting model used for prediction in scenario A"))

print(expect_equal(ch_modA$control$ne$weights, mat_array,
                   info = "Model setup for prediction is correct"))

print(expect_false(isTRUE(all.equal(ch_modA, ch_modB)),
                   info = "Model setup for prediction is correct"))

print(expect_equal(ch_modB$control$ne$weights, sc_mat_array,
                   info = "Different weights used for prediction in scenario B"))