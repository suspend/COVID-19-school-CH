# test the grouping variable
run_test("Grouping variable is constructed correctly based on age limits given in setup.R", {
  expect_equal_wrapper(sum(grouping), dm)
})
