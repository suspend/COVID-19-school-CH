# Load libraries --------------------------------------------------------------------------
library(hhh4addon)
library(parallel)
library(MASS)

# define function --------------------------------------------------------------------------
# Add terms to an existing formula
# This only checks whether symbols exist in global environment.
# It only works for formulas which have a right hand side only
# Also, if expressions are used in the formula, insert them as character string
add_to_formula <- function(...){
  args <- as.list(substitute(...()))
  args <- vapply(args, function(x){
    # if symbol is not a variable (return it as character)
    if(!(deparse(x) %in% ls(envir = .GlobalEnv))){
      return(deparse(x))
    }
    # if symbol refers to a formula: convert to character and return right hand side (RHS) of formula
    if(inherits(eval(x), "formula")){
      char <- as.character(eval(x))
      return(char[length(char)])
    }
    # if symbol refers to a character vector, return the character vector
    if(is.character(eval(x))){
      return(eval(x))
    }
    # in all other cases (symbol refers to a numeric vector or matrix): return symbol as a character
    return(deparse(x))
  }, character(1L))
  # paste everything together and convert to a formula
  as.formula(paste0("~", paste(args, collapse = " + ")), env = .GlobalEnv)
}

# create formula grid --------------------------------------------------------------------------

# We always include
# age, public holidays, and school holidays in our model (leave school holidays out for now)
age_epi <- add_to_formula(fe(1, unitSpecific = TRUE))
age_end <- add_to_formula(fe(1, which = c(rep(TRUE, 5), FALSE)))
wkday <- add_to_formula(wkd_tu, wkd_we, wkd_th, wkd_fr, wkd_sa, wkd_su)

# If school holidays should be covariates, add them to end and epi
end <- add_to_formula(-1, age_end, plh)
epi <- add_to_formula(-1, age_epi, plh)

# Model 0 -- reference model
component_00_end <- end
component_00_epi <- epi #ne in hhh4
# Model 1 -- day of the week
component_01_end <- add_to_formula(end, wkday)
component_01_epi <- add_to_formula(epi, wkday)
# Model 2 -- weekend
component_02_end <- add_to_formula(end, wkend)
component_02_epi <- add_to_formula(epi, wkend)
# Model 3 -- time trend
component_03_end <- add_to_formula(end, time_trend)
component_03_epi <- add_to_formula(epi, time_trend)
# Model 4 -- testing
component_04_end <- add_to_formula(end, test)
component_04_epi <- add_to_formula(epi, test)
# Model 5 -- testing and day of the week
component_05_end <- add_to_formula(end, test, wkday)
component_05_epi <- add_to_formula(epi, test, wkday)
# Model 6 -- testing and weekend
component_06_end <- add_to_formula(end, test, wkend)
component_06_epi <- add_to_formula(epi, test, wkend)
# Model 7 -- testing and time trend
component_07_end <- add_to_formula(end, test, time_trend)
component_07_epi <- add_to_formula(epi, test, time_trend)
# Model 8 -- temperature
component_08_end <- add_to_formula(end, temp)
component_08_epi <- add_to_formula(epi, temp)
# Model 9 -- temperature and day of the week
component_09_end <- add_to_formula(end, temp, wkday)
component_09_epi <- add_to_formula(epi, temp, wkday)
# Model 10 -- temperature and weekend
component_10_end <- add_to_formula(end, temp, wkend)
component_10_epi <- add_to_formula(epi, temp, wkend)
# Model 11 -- temperature and time trend
component_11_end <- add_to_formula(end, temp, time_trend)
component_11_epi <- add_to_formula(epi, temp, time_trend)
# Model 12 -- seasonality
component_12_end <- addSeason2formula(end, S = 1, period = 365)
component_12_epi <- addSeason2formula(epi, S = 1, period = 365)
# Model 13 -- seasonality and day of the week
component_13_end <- addSeason2formula(add_to_formula(end, wkday), S = 1, period = 365)
component_13_epi <- addSeason2formula(add_to_formula(epi, wkday), S = 1, period = 365)
# Model 14 -- seasonality and weekend
component_14_end <- addSeason2formula(add_to_formula(end, wkend), S = 1, period = 365)
component_14_epi <- addSeason2formula(add_to_formula(epi, wkend), S = 1, period = 365)
# Model 15 -- seasonality and time trend
component_15_end <- addSeason2formula(add_to_formula(end, time_trend), S = 1, period = 365)
component_15_epi <- addSeason2formula(add_to_formula(epi, time_trend), S = 1, period = 365)

end_opts <- list(component_00_end,
                 component_01_end,
                 component_02_end,
                 component_03_end,
                 component_04_end,
                 component_05_end,
                 component_06_end,
                 component_07_end,
                 component_08_end,
                 component_09_end,
                 component_10_end,
                 component_11_end,
                 component_12_end,
                 component_13_end,
                 component_14_end,
                 component_15_end)

epi_opts <- list(component_00_epi,
                 component_01_epi,
                 component_02_epi,
                 component_03_epi,
                 component_04_epi,
                 component_05_epi,
                 component_06_epi,
                 component_07_epi,
                 component_08_epi,
                 component_09_epi,
                 component_10_epi,
                 component_11_epi,
                 component_12_epi,
                 component_13_epi,
                 component_14_epi,
                 component_15_epi)

model_options <- expand.grid(endemic = end_opts, 
                             epidemic = epi_opts,
                             #ne_weights = paste0("mat_array_", c("a", "b", "c")),
                             stringsAsFactors = FALSE)

# check grid --------------------------------------------------------------------------

# Check whether each formula in end_opts appears exactly the
# same number of times in model_options$endemic
n_times_end <- vapply(end_opts, function(x){
  sum(vapply(model_options$endemic, function(y){
    in_x_but_not_y <- length(setdiff(attr(terms(x), "term.labels"), attr(terms(y), "term.labels")))
    in_y_but_not_x <- length(setdiff(attr(terms(y), "term.labels"), attr(terms(x), "term.labels")))
    if(in_x_but_not_y == 0L & in_y_but_not_x == 0L){
      return(TRUE)
    } else {
      return(FALSE)
    }
  }, logical(1L)))
}, integer(1L))
all(n_times_end == n_times_end[1])
# Check whether each formula in epi_opts appears exactly the
# same number of times in model_options$epidemic
n_times_epi <- vapply(epi_opts, function(x){
  sum(vapply(model_options$epidemic, function(y){
    in_x_but_not_y <- length(setdiff(attr(terms(x), "term.labels"), attr(terms(y), "term.labels")))
    in_y_but_not_x <- length(setdiff(attr(terms(y), "term.labels"), attr(terms(x), "term.labels")))
    if(in_x_but_not_y == 0L & in_y_but_not_x == 0L){
      return(TRUE)
    } else {
      return(FALSE)
    }
  }, logical(1L)))
}, integer(1L))
all(n_times_epi == n_times_epi[1])
# Check whether epi and end formulas appear the same number of times
all(n_times_end == n_times_epi)

# NB reason for constructing the models this way instead of using the
# update function is that if a convergence problem occurs in a model
# which is to have the update function applied to it, the update may
# fail and this fail might not be immediately obvious (safeguarding)

# clean up --------------------------------------------------------------------------

# Remove unnecessary variables
rm(list = ls(pattern = "^component_\\d{2}_[epi|end]"))
rm(end_opts, epi_opts,
   age_end, age_epi, wkday,
   end, epi,
   n_times_end, n_times_epi,
   add_to_formula)

# load data --------------------------------------------------------------------------

# Load covariates and ne_weights
load("covariate_options.Rda")
load("data/neweights.Rda")

source("setup.R") # As "mxl" and "optim_opts" variables are defined there
#day <- time
#rm(time)

# fit models --------------------------------------------------------------------------
models <- lapply(weights, function(y){
  mods_idea <- parallel::mclapply(seq_len(nrow(model_options)), function(x){
    start <- Sys.time()
    mod <- profile_par_lag(sts_obj,
                           control =
                             list(end = list(f = model_options$endemic[[x]],
                                             offset = population(sts_obj)),
                                  ne = list(f = model_options$epidemic[[x]],
                                            weights = y$scenario_a),
                                  family = "NegBinM",
                                  funct_lag = poisson_lag,
                                  max_lag = mxl,
                                  subset = 15 : nrow(sts_obj),
                                  normalize = FALSE,
                                  optimizer = optim_opts,
                                  keep.terms = TRUE))
    used_time <- round(difftime(Sys.time(), start, units = "secs"), 3)
    system(paste0("echo ", "'Fitted model: ", x, " of ", nrow(model_options), " in ", used_time, " seconds.'"))
    components <- vapply(c("end", "ne", "ar"), function(x) mod$control[[x]]$inModel, logical(1L))
    components <- names(components)[components]
    form_terms <- lapply(components, function(x){
      trms <- attr(terms(mod$control[[x]]$f), "term.labels")
      trms <- gsub("^fe\\s*\\(\\s*1\\s*,.+\\)$", "Age", trms)
      trms <- gsub("^plh$", "Public holidays", trms)
      #out <- gsub("^scl$", "School holidays", out)
      if(all(c("wkd_tu", "wkd_we", "wkd_th", "wkd_fr", "wkd_sa", "wkd_su") %in% trms)){
        idx <- which(trms %in% c("wkd_tu", "wkd_we", "wkd_th", "wkd_fr", "wkd_sa", "wkd_su"))
        trms[idx[1]] <- "Weekday"
        trms <- trms[-idx[2:length(idx)]]
      }
      trms <- gsub("^time_trend$", "Time trend", trms)
      trms <- gsub("^wkend$", "Weekend", trms)
      trms <- gsub("^test$", "Testing rate", trms)
      trms <- gsub("^temp$", "Temperature", trms)
      if(any(grepl("^(sin|cos)\\s*\\(", trms))){
        idx <- which(grepl("^(sin|cos)\\s*\\(", trms))
        n_seasonal_terms <- length(idx)/2
        trms[idx[1]] <- paste0("Seasonality (", n_seasonal_terms, ")")
        trms <- trms[-idx[2:length(idx)]]
      }
      return(trms)
    })
    names(form_terms) <- components
    res <- list(model = mod,
                id = x,
                BIC = tryCatch( { BIC(mod) }, 
                                warning = function(w){ capture.output(w) },
                                error = function(e){ capture.output(e) }),
                AIC = tryCatch( { AIC(mod) }, 
                                warning = function(w){ capture.output(w) },
                                error = function(e){ capture.output(e) }),
                covariates = form_terms,
                messages = NA_character_)
    if(is.character(res$BIC)){
      res$messages <- list(BIC = res$BIC)
      res$BIC <- NA_real_
    }
    if(is.character(res$AIC)){
      if(is.na(res$messages)){
        res$messages <- list(AIC = res$AIC)
        res$AIC <- NA_real_
      } else {
        res$messages <- append(res$messages, list(AIC = res$AIC))
        res$AIC <- NA_real_
      }
    }
    return(res)
  }, 
  mc.preschedule = FALSE, 
  mc.cores = parallel::detectCores(), 
  mc.cleanup = TRUE)
})

save(models, file = "models.Rda")

# find best model for each weight construction method --------------------------------------------------- 

# Find the best model for scenario A of each method
crit <- c("AIC", "BIC")
grid <- expand.grid(crit = crit, model = names(models),
                    stringsAsFactors = FALSE)
grid$name <- with(grid, paste0(model, "_", crit))

best_mods <- lapply(seq_len(nrow(grid)), function(x){
  idx <- order(vapply(models[[grid$model[x]]], `[[`, i = grid$crit[x], numeric(1L)),
               decreasing = FALSE, na.last = TRUE)[1L]
  mod <- models[[grid$model[x]]][[idx]]
  mod
})
names(best_mods) <- grid$name

grid$best_model_id <- vapply(best_mods, `[[`, i = "id", integer(1L))

save(grid, file = "model_overview.Rda")

# calculate trajectories for scenarios within weight methods ---------------------------------

# calculate trajectories for each of the other scenarios
predictions <- lapply(seq_along(best_mods), function(x){
  scenarios <- c("a", "b", "c")
  baseline_name <- "a"
  a <- best_mods[[x]]$model
  b <- c <- a
  b$control$ne$weights <- weights[[grid$model[x]]]$scenario_b
  c$control$ne$weights <- weights[[grid$model[x]]]$scenario_c
  b$terms <- c$terms <- NULL # Fix from JB
  predictions <- lapply(scenarios, function(y){
    assign("mod", eval(as.name(y)))
    pred <- predictive_moments(mod,
                               t_condition = sta_tm,
                               lgt = nrow(mod$stsObj) - sta_tm,
                               return_Sigma = FALSE,
                               return_cov_array = FALSE,
                               return_mu_decomposed = FALSE)
    if(any(is.na(pred$mu_matrix))) 
      stop(paste0("There are NAs in the predictions of scenario ", y, "."))
    if(any(is.na(pred$mu_matrix))) 
      stop(paste0("There are NAs in the predictions of scenario ", y, "."))
    if(any(is.na(pred)))
      stop(paste0("There are NAs in the predictions of scenario ", y, "."))
    list(prediction = pred, 
         date = surveillance::epoch(mod$stsObj)[pred$timepoints])
  })
  names(predictions) <- scenarios
  
  # calculate comparisons between scenarios
  comparisons <- lapply(scenarios, function(x){
    if(x == baseline_name) return(list(data = NA, plot = NA))
    df <- dplyr::as_tibble(predictions[[x]]$prediction$mu_matrix / predictions[["a"]]$prediction$mu_matrix)
    df$date <- predictions[[x]]$date
    df <- tidyr::pivot_longer(df, cols = colnames(df)[colnames(df) != "date"])
    p <- ggplot2::ggplot(df, ggplot2::aes(x = date, y = value, colour = name)) +
      ggplot2::geom_line() +
      ggplot2::labs(y = "Increase in COVID-19 cases",
                    x = "", colour = "Age group") +
      ggplot2::theme(legend.position = "bottom") +
      ggplot2::coord_trans(y = "log10") +
      ggplot2::scale_x_date(date_breaks = "1 month", date_labels = "%b")
    list(data = df, plot = p)
  })
  names(comparisons) <- paste0("scenario_", scenarios)
  
  out <- lapply(predictions, `[[`, i = "prediction")
  out <- lapply(seq_along(out), function(x){
    list(prediction = out[[x]], comparison = comparisons[[x]])
  })
  names(out) <- scenarios
  out
})
names(predictions) <- names(best_mods)

save(predictions, file = "predictions.Rda")

# Predict with simulated coefficients and weights -------------------------------------------------------------

# load simulated weights

load("data/sim_neweights.Rda")

# loop over each construction method and criterion
# however, if construction method is the same and model id is the same, do this only once

sim_mod_ids <- lapply(split(grid, factor(grid$model, levels = unique(grid$model)),
                            drop = TRUE,
                            lex.order = FALSE), 
                      function(x) unique(x$best_model_id))

if(!all(names(sim_mod_ids) == names(sim_weights)))
  stop("Check list order. There seems to be something wrong.")

model_simulations <- lapply(seq_along(sim_mod_ids), function(x) { # x is the weight construction method
  
  # what models do we need to perform this stuff on
  method <- names(sim_mod_ids)[x]
  model_id <- sim_mod_ids[[method]]
  # what are the simulated weights
  wts <- sim_weights[[method]]
  
  out <- lapply(seq_along(model_id), function(y) { # y is the model id of the best model(s) under that method
    
    # get the model
    mod_id <- model_id[y]
    mod <- models[[method]][[mod_id]]
    if(mod$id != mod_id)
      stop("You got the wrong model. We would want model with id ", mod_id,
           " but we got model with id ", mod$id, ".")
    mod <- mod$model
    # sample coefficients
    coef_list <- lapply(seq_len(nsim), function(idx){
      set.seed(idx) # Reproducibility
      mvrnorm(n = 1, mu = mod$coefficients, Sigma = mod$cov)
    })
    # precompute variables needed often in the following loop
    n_rows <- nrow(mod$stsObj) - sta_tm
    n_cols <- ncol(mod$stsObj)
    
    # run the 10'000 simulations
    out <- parallel::mclapply(seq_len(nsim), function(z, checks){ # z is the index for the number of simulations
      
      # start timer
      start_time <- Sys.time()
      
      # Copy best model
      modA <- modB <- modC <- mod
      # Set sampled coefficients for A & B & C
      modA$coefficients <- modB$coefficients <- modC$coefficients <- coef_list[[x]] # A, B and C are the same here
      # Set simulated weights
      modA$control$ne$weights <- wts$scenario_a[[z]]
      modB$control$ne$weights <- wts$scenario_b[[z]]
      modC$control$ne$weights <- wts$scenario_c[[z]]
      
      # Apply fix
      modA$terms <- modB$terms <- modC$terms <- NULL
      
      # Predict
      mom_A <- predictive_moments(modA,
                                  t_condition = sta_tm,
                                  lgt = n_rows,
                                  return_Sigma = FALSE,
                                  # Increases speed slightly
                                  return_cov_array = FALSE)
      mom_B <- predictive_moments(modB,
                                  t_condition = sta_tm,
                                  lgt = n_rows,
                                  return_Sigma = FALSE,
                                  # Increases speed slightly
                                  return_cov_array = FALSE)
      mom_C <- predictive_moments(modC,
                                  t_condition = sta_tm,
                                  lgt = n_rows,
                                  return_Sigma = FALSE,
                                  # Increases speed slightly
                                  return_cov_array = FALSE)
      
      # checks
      if(checks){
        if(any(is.na(mom_A$mu_matrix))){
          stop("Check method ", method, ", model ", mod_id, 
               " prediction of mom_A on index ", z)
        }
        if(any(is.na(mom_B$mu_matrix))){
          stop("Check method ", method, ", model ", mod_id, 
                  " prediction of mom_B on index ", z)
        }
        if(any(is.na(mom_C$mu_matrix))){
          stop("Check method ", method, ", model ", mod_id, 
               " prediction of mom_C on index ", z)
        }
      }
      
      # Set rownames of mu_matrix
      #rownames(mom_A$mu_matrix) <- rownames(mom_B$mu_matrix) <- rnms_mumat
      
      # Print timing
      end_time <- Sys.time()
      system(sprintf('echo "%s\n"', paste0("Prediction of method '", method,
                                           "' model ", y, "/", length(model_id),
                                           " simulation ", z, " took: ", end_time - start_time)))
      
      return(list(A = mom_A$mu_matrix, B = mom_B$mu_matrix, C = mom_C$mu_matrix))
    },
    checks = TRUE,
    mc.cores = parallel::detectCores() - 1L,
    mc.preschedule = FALSE,
    mc.cleanup = TRUE
    )
    
    # Extract moments A, B and C
    mom_array_A_mean <- vapply(out, `[[`, i = "A", matrix(NA_real_, nrow = n_rows, ncol = n_cols))
    mom_array_B_mean <- vapply(out, `[[`, i = "B", matrix(NA_real_, nrow = n_rows, ncol = n_cols))
    mom_array_C_mean <- vapply(out, `[[`, i = "C", matrix(NA_real_, nrow = n_rows, ncol = n_cols))
    
    # Set dimension names
    dimnames(mom_array_A_mean) <- 
      dimnames(mom_array_B_mean) <- 
      dimnames(mom_array_C_mean) <- list(date = tail(as.character(case_dates), n_rows),
                                         age_group = labls,
                                         simulation = seq_len(nsim))
    
    list(A = mom_array_A_mean, B = mom_array_B_mean, C = mom_array_C_mean)
  })
  names(out) <- paste0("model_", as.character(model_id))
  out
})
names(model_simulations) <- names(sim_mod_ids)

# Save
save(model_simulations, file = "model_simulations.Rda")
